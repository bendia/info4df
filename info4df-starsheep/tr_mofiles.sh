#!/bin/bash
# création de .mo
# info4df
echo ""
echo "Suppression des anciens fichiers *.po"
echo ""
find locale -name "info4df.po~" -exec rm {} \;
echo ""
echo "Génération des .mo"
echo ""
cd locale/en/LC_MESSAGES && msgfmt info4df.po -o info4df.mo
cd ../../fr/LC_MESSAGES && msgfmt info4df.po -o info4df.mo
echo ""
echo -n ".mo générés ! [Enter] pous quitter"
read anykey
exit 0

