#!/usr/bin/env python
# -*- coding:Utf-8 -*-

"""
Developers :                 thuban (thuban@yeuxdelibad.net)
                             arpinux (arpinux@member.fsf.org)
                             Starsheep (starsheep@openmailbox.org)
                             FirePowi
                             Michael Gebetsroither <michael@mgeb.org> (paste part)
                             
Licence :            GNU General Public Licence v3

Description :           Tool to list a description of the system
                        to improve help on forums in case of trouble.
"""


import os
import sys
import io
import subprocess
import gettext
from lib.export import bb

appname="info4df"

#i18n
#~ gettext.bindtextdomain(appname, '/usr/share/locale')
gettext.bindtextdomain(appname, 'locale') # Use for local tests
gettext.textdomain(appname)
_ = gettext.gettext


def read_as_utf8(fileno):
    fp = io.open(fileno, mode="r", encoding="utf-8", closefd=False)
    txt = fp.read()
    fp.close()
    return txt

class Cmd:
    def __init__(self,cmd,info):
        self.cmd = cmd
        self.info = info
        
    def get_info(self):
        return self.info
        
    def read(self):
        return os.popen(self.cmd).read()
        
    def read_as_root(self):
        txt = ""
        p = subprocess.Popen(["pkexec env DISPLAY=$DISPLAY XAUTHORITY=$XAUTHORITY "+self.cmd], shell=True, stdout=subprocess.PIPE)
        txt = read_as_utf8(p.stdout.fileno())
        return txt


class FileInfo:
    def __init__(self,name,info):
        self.name = name
        self.info = info
        
    def get_info(self):
        return self.info
        
    def read(self):
      if os.path.isfile(self.name):
          with open(self.name, 'r') as hi:
              return hi.read()
      else:
          return "unknown"
          
          
class DirInfo:
    def __init__(self,directory,info,ext=""):
        self.directory = directory
        self.info = info
        self.ext = ext
        
    def get_info(self):
        return self.info
        
    def read(self):
        info = ""
        if os.path.isfile(self.directory):
            with open(self.directory, 'r') as s:
                info = self.directory+":\n{}".format(s.read())
        if os.path.isdir(self.directory+'.d'):
            for fileinfo in os.listdir(self.directory+'.d'):
                if self.ext == "":
                    with open(os.path.join(self.directory+'.d',fileinfo), 'r') as s:
                        info += "#"+self.directory+".d/{}\n{}".format(fileinfo,s.read())
                else:
                    if fileinfo.endswith(self.ext):
                        with open(os.path.join(self.directory+'.d',fileinfo), 'r') as s:
                            info += "#"+self.directory+".d/{}\n{}".format(fileinfo,s.read())
        return info


class cmd_list:
    # Information on the system
    debver = FileInfo("/etc/debian_version",_("Your Debian version"))
    host = Cmd("uname -n",_("Name of the distribution"))
    kernel = Cmd("uname -sr",_("Kernel version"))
    sources = DirInfo('/etc/apt/sources.list',_("Package sources"),'.list')
    preferences = DirInfo('/etc/apt/preferences',_("Pinning preferences"))
    
    # Information on the hardware
    pci = Cmd("lspci",_("List of internal peripherals"))
    usb = Cmd("lsusb",_("List of connected peripherals"))
    hw = Cmd("dmidecode -q -t bios -t system -t processor -t memory",_("Hardware informations"))
    scr = Cmd("xrandr",_("Monitor configuration"))
    
    # Information on the network
    netinterfaces = FileInfo('/etc/network/interfaces',_("List of network interfaces"))
    resolvconf = FileInfo('/etc/resolv.conf',_("Configuration of the DNS resolver"))
    ifconfig = Cmd("/sbin/ifconfig -a",_("Network configuration"))
    route = Cmd("/sbin/route -n",_("IP routing table"))
    
    #information on the storage
    fs = Cmd("df -h",_("Disk use"))
    part = Cmd("/sbin/fdisk -l",_("Disk partitioning"))
    fstab = FileInfo("/etc/fstab",_("File system table"))


def get_distro():
    txt = bb(cmd_list.debver.read(), cmd_list.debver.get_info())
    txt += bb(cmd_list.host.read(), cmd_list.host.get_info())
    txt += bb(cmd_list.kernel.read(), cmd_list.kernel.get_info())
    txt += bb(cmd_list.sources.read(), cmd_list.sources.get_info(), "apt_sources")
    txt += bb(cmd_list.preferences.read(), cmd_list.preferences.get_info())
    return(txt)
 
 
def get_hw():
    txt = bb(cmd_list.pci.read(), cmd_list.pci.get_info())
    txt += bb(cmd_list.usb.read(), cmd_list.usb.get_info())
    txt += bb(cmd_list.hw.read_as_root(), cmd_list.hw.get_info())
    txt += bb(cmd_list.scr.read(), cmd_list.scr.get_info())
    return(txt)
 
 
def get_network():
    txt = bb(cmd_list.netinterfaces.read(), cmd_list.netinterfaces.get_info())
    txt += bb(cmd_list.resolvconf.read(), cmd_list.resolvconf.get_info())
    txt += bb(cmd_list.ifconfig.read(), cmd_list.ifconfig.get_info())
    txt += bb(cmd_list.route.read(), cmd_list.route.get_info())
    return(txt)
 
 
def get_disks():
    txt = bb(cmd_list.fs.read(), cmd_list.fs.get_info())
    txt += bb(cmd_list.part.read_as_root(), cmd_list.part.get_info())
    txt += bb(cmd_list.fstab.read(), cmd_list.fstab.get_info())
    return(txt)


def get_all():
    txt = get_distro()
    txt += get_hw()
    txt += get_network()
    txt += get_disks()
    return(txt)


# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4
