#!/usr/bin/env python
# -*- coding:Utf-8 -*-

"""
Developers :                 thuban (thuban@yeuxdelibad.net)
                             arpinux (arpinux@member.fsf.org)
                             Starsheep (starsheep@openmailbox.org)
                             FirePowi
                             Michael Gebetsroither <michael@mgeb.org> (paste part)
                             
Licence :            GNU General Public Licence v3

Description :           Tool to list a description of the system
                        to improve help on forums in case of trouble.
"""


import sys
import gettext
if sys.version[0] == "2":
        from urllib import urlencode
        from urllib import FancyURLopener
else:
        from urllib.parse import urlencode
        from urllib.request import FancyURLopener
        
appname="info4df"

#i18n
#~ gettext.bindtextdomain(appname, '/usr/share/locale')
gettext.bindtextdomain(appname, 'locale') # Use for local tests
gettext.textdomain(appname)
_ = gettext.gettext


def safe_unicode(obj, *args):
    """ return the unicode representation of obj """
    try:
        return unicode(obj, *args)
    except UnicodeDecodeError:
        # obj is byte string
        ascii_text = str(obj).encode('string_escape')
        return unicode(ascii_text)
        

def upload_code(code):
        """upload code to paste.debian.net
        return the page url or False
        """
        DEFAULT_SERVER='https://paste.debian.net/'
        
        user = "DFrocks"
        expire = -1
        lang = "text"
        private = 0
        
        params = { "poster":user, "expire":expire, "lang":lang, "private":private, "code":code.encode('utf-8') }
        params = urlencode(params)

        url_opener = FancyURLopener()
        try:
            page = url_opener.open(DEFAULT_SERVER, params)
            #page_url = page.read().decode('utf-8').strip()
            return(page.url)
        except Exception as e:
            print(_("Failed to uplad code to pastebin: \n{}").format(e)) 
            return(False)


def paste_code(code):
    paste_url = upload_code(code)
    if paste_url:
        import webbrowser
        webbrowser.open(paste_url)
        return(paste_url)
        

def bb(txt, title="",codetype=""):
    if len(txt) > 0:
        # put txt in bbcode
        if codetype != "":
            out = "--{}--\n[code={}]{}[/code]\n".format(title,codetype,txt)
        elif sys.version[0] == "2":
            out = safe_unicode("--{}--\n[code]{}[/code]\n").format(safe_unicode(title),safe_unicode(txt))
        else:
            out = "--{}--\n[code]{}[/code]\n".format(title,txt)
    else:
        out = ""
    return(out)
 
