#!/bin/bash
# update translation script
# info4df
echo ""
echo "générer le .pot"
echo ""
pygettext -o locale/info4df.pot lib/cmd.py lib/export.py ui/cli.py ui/web.py
#~ /usr/lib/python3.5/Tools/i18n/pygettext.py -o locale/info4df.pot lib/cmd.py lib/export.py ui/cli.py ui/web.py
cd locale
echo ""
echo "mise à jour des fichiers de traduction"
echo ""
msgmerge --update en/LC_MESSAGES/info4df.po info4df.pot
msgmerge --update fr/LC_MESSAGES/info4df.po info4df.pot
echo ""
echo "suppression des .mo obsolètes"
echo ""
find . -name "info4df.mo" -exec rm {} \;
echo ""
echo "édition des fichiers : @toi de jouer. reviens et lance le script de génération des .mo"
echo -n "[Enter] pour quitter"
read anykey
exit 0

