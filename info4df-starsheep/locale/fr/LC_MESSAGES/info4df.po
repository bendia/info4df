# French translations for info package
# Traductions françaises du paquet info.
# Copyright (C) 2016 ORGANIZATION
# Sylvain Treutenaere <starsheep@openmailbox.org>, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: info 4df-starsheep\n"
"POT-Creation-Date: 2016-07-29 18:43+0200\n"
"PO-Revision-Date: 2016-07-15 19:06+0200\n"
"Last-Translator: Sylvain Treutenaere <starsheep@openmailbox.org>\n"
"Language-Team: French\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: pygettext.py 1.5\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: lib/cmd.py:102
msgid "Your Debian version"
msgstr "Votre version de Debian"

#: lib/cmd.py:103
msgid "Name of the distribution"
msgstr "Nom de la distribution"

#: lib/cmd.py:104
msgid "Kernel version"
msgstr "Version du noyau"

#: lib/cmd.py:105
msgid "Package sources"
msgstr "Source des paquets"

#: lib/cmd.py:106
msgid "Pinning preferences"
msgstr "Préférences pour le pinning"

#: lib/cmd.py:109
msgid "List of internal peripherals"
msgstr "Liste des périphériques internes"

#: lib/cmd.py:110
msgid "List of connected peripherals"
msgstr "Liste des périphériques connectés"

#: lib/cmd.py:111
msgid "Hardware informations"
msgstr "Informations sur le matériel"

#: lib/cmd.py:112
msgid "Monitor configuration"
msgstr "Configuration des écrans"

#: lib/cmd.py:115
msgid "List of network interfaces"
msgstr "Liste des interfaces réseau"

#: lib/cmd.py:116
msgid "Configuration of the DNS resolver"
msgstr "Configuration du résolveur DNS"

#: lib/cmd.py:117
msgid "Network configuration"
msgstr "Configuration du réseau"

#: lib/cmd.py:118
msgid "IP routing table"
msgstr "Table de routage IP"

#: lib/cmd.py:121
msgid "Disk use"
msgstr "Utilisation du disque"

#: lib/cmd.py:122
msgid "Disk partitioning"
msgstr "Partitionnement du disque"

#: lib/cmd.py:123
msgid "File system table"
msgstr "Table du système de fichiers"

#: lib/export.py:66
msgid ""
"Failed to uplad code to pastebin: \n"
"{}"
msgstr ""
"Échec de l'envoi du code sur le pastebin: \n"
"{}"

#: ui/cli.py:45
msgid ""
"This application obtains information about your Debian GNU/Linux system to "
"get help on forums."
msgstr ""
"Cette page vous permettra de transmettre des informations à propos de votre "
"système Debian GNU/Linux pour obtenir de l'aide sur les forums."

#: ui/cli.py:46
msgid "According to your choice, your session password may be asked."
msgstr ""
"Selon votre choix, le mot de passe superadministrateur (root) pourra vous "
"être demandé."

#: ui/cli.py:47
msgid "The answer is pre-formatted, you can easily copy/paste it on forums."
msgstr ""
"Le texte sera pré-formaté, il vous suffit de le copier/coller sur le forum."

#: ui/cli.py:48
msgid ""
"You can also provide the pastebin link (if asked): it points on the report."
msgstr ""
"Vous pouvez aussi donner le lien qui vous sera donné : il pointe vers le "
"compte-rendu"

#: ui/cli.py:68
msgid "What information are you interested in?"
msgstr "Quelles informations souhaitez vous ?"

#: ui/cli.py:70
msgid "information on the system"
msgstr "informations sur la distribution"

#: ui/cli.py:71
msgid "information on the hardware"
msgstr "informations sur le matériel"

#: ui/cli.py:72
msgid "information on the network"
msgstr "informations sur le réseau"

#: ui/cli.py:73
msgid "information on the storage"
msgstr "informations sur les disques"

#: ui/cli.py:74
msgid "all information listed above"
msgstr "toutes les informations listées ci-dessus"

#: ui/cli.py:76
msgid "Confirm with the Enter key"
msgstr "Validez votre choix avec Entrée"

#: ui/cli.py:89
msgid "Unknown choice"
msgstr "Choix inconnu"

#: ui/cli.py:92
msgid "Please find below the report to copy/paste on forums"
msgstr "Voici le code à copier/coller sur le forum"

#: ui/cli.py:97
msgid ""
"Would you like upload the report on internet and obtain the URL address ? "
"(on a pastebin) [y/n]"
msgstr ""
"Voulez vous coller ces informations en ligne et récupérer le lien ? (sur un "
"pastebin) [o/N]"

#: ui/cli.py:98
msgid "yes"
msgstr "oui"

#: ui/cli.py:101
msgid "Copy this URL address below which links to your report on internet:"
msgstr "Copiez ce lien qui contient toutes les informations ci-dessus :"

#: ui/cli.py:104
msgid "Press Enter to exit..."
msgstr "Appuyez sur Entrée pour quitter..."

#: ui/web.py:35
msgid "Usage:"
msgstr "Utilisation :"

#: ui/web.py:35
msgid "options"
msgstr "options"

#: ui/web.py:36
msgid "Display information in a web browser"
msgstr "Affiche les informations dans un navigateur internet"

#: ui/web.py:87
msgid ""
"The following infomation are already formatted. All you need to do is to "
"copy/paste in your forum message."
msgstr ""
"Les informations ci-dessous sont déjà pré-formatées. Il ne vous reste plus "
"qu'à les copier/coller dans votre message sur le forum."

#: ui/web.py:88
msgid ""
"You can also upload this inofmation on a pastebin by clicking on the "
"following button:"
msgstr ""
"Vous pouvez aussi envoyer le rapport complet sur le pastebin de debian en "
"cliquant sur ce bouton :"

#: ui/web.py:89
msgid "Upload on paste.debian.net"
msgstr "Envoyer sur paste.debian.net"

#: ui/web.py:91
msgid "Info4df report"
msgstr "Rapport info4df"

#: ui/web.py:93
msgid "Installation"
msgstr "Installation"

#: ui/web.py:94
msgid "Hardware"
msgstr "Matériel"

#: ui/web.py:95
msgid "Network"
msgstr "Réseau"

#: ui/web.py:96
msgid "Storage"
msgstr "Stockage"

#: ui/web.py:97
msgid "Complete report"
msgstr "Rapport complet"
