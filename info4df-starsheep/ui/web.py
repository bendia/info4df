#!/usr/bin/env python
# -*- coding:Utf-8 -*-

"""
Developers :                 thuban (thuban@yeuxdelibad.net)
                             arpinux (arpinux@member.fsf.org)
                             Starsheep (starsheep@openmailbox.org)
                             FirePowi
                             Michael Gebetsroither <michael@mgeb.org> (paste part)
                             
Licence :            GNU General Public Licence v3

Description :           Tool to list a description of the system
                        to improve help on forums in case of trouble.
"""


import sys
import gettext
from lib.cmd import *
from lib.export import *
import webbrowser
import tempfile

appname="info4df"

#i18n
#~ gettext.bindtextdomain(appname, '/usr/share/locale')
gettext.bindtextdomain(appname, 'locale') # Use for local tests
gettext.textdomain(appname)
_ = gettext.gettext


def help():
    print(""+_("Usage:")+" {} <".format(sys.argv[0])+_("options")+">")
    print("           --gui    :    "+_("Display information in a web browser"))

def write_css():
    with open("ui/libweb/style.css", "r") as f:
        css = f.read()
    txt = '<style type="text/css">\n'+css+'</style>'
    return txt

def write_field(title,content):
    with open("ui/libweb/field.html", "r") as f:
        html = f.read()
    txt = html.format(title,content)
    return txt

def write_header(title,css,info,btn_info,btn_msg):
    with open("ui/libweb/header.html", "r") as f:
        html = f.read()
    txt = html.format(title,css,title,info,btn_info,btn_msg)
    return txt

def start_form():
    txt = '<form action="https://paste.debian.net/" method="post" >'
    return txt

def end_form():
    return """
    <input name="poster" type="hidden" value="DFrocks">        
    <input name="expire" type="hidden" value="-1">        
    <input name="lang" type="hidden" value="text">        
    <input name="private" type="hidden" value=0>        

    </form>
    """
    
def write_footer():
    return """
    </main>
    </body>
    </html>
    """

def web():
    # Open temporary file
    htmlfile = tempfile.mkstemp(prefix="info4df", suffix=".html")[1]
    # Get information
    distro_info = get_distro()
    hw_info = get_hw()
    nw_info = get_network()
    disks_info = get_disks()
    all_info = "\n".join([distro_info, hw_info, nw_info, disks_info])
    # information messages
    info = _("The following infomation are already formatted. All you need to do is to copy/paste in your forum message.")
    btn_info = _("You can also upload this inofmation on a pastebin by clicking on the following button:")
    btn_msg = _("Upload on paste.debian.net")
    # Create html
    html = write_header(_("Info4df report"),write_css(),info,btn_info,btn_msg)
    html += start_form()
    html += write_field(_("Installation"),distro_info)
    html += write_field(_("Hardware"),hw_info)
    html += write_field(_("Network"),nw_info)
    html += write_field(_("Storage"),disks_info)
    html += write_field(_("Complete report"),"\n".join([distro_info, hw_info, nw_info, disks_info]))
    html += end_form()
    html += write_footer()
    # Write html
    with open(htmlfile, "w") as out:
        out.write(html)
    webbrowser.open('file://{}'.format(htmlfile))

