#!/usr/bin/env python
# -*- coding:Utf-8 -*-

from info4df import *
import webbrowser
import tempfile

tpl="""
<!doctype html>
<html>
<head>
<title>info4df : rapport</title>
<meta charset="UTF-8">
<meta name="author" content="Badwulf" />
<meta name="viewport" content="width=device-width, initial-scale=1">
{}
</head>
<body>
<main>
<h1>Rapport info4df</h1>
<p>Les informations ci-dessous sont déjà pré-formatées. Il ne vous reste plus qu'à les copier/coller dans votre message sur le forum.</p>

<form action="https://paste.debian.net/" method="post" >

<div>
    Vous pouvez aussi envoyer le rapport complet sur le pastebin de debian en cliquant sur ce bouton : 
    <input name="send" type="submit" value="Envoyer sur paste.debian.net">
</div>

<hr>

<form action="https://paste.debian.net/" method="post" >
<fieldset>
<legend>Votre installation</legend>
<div>
    <textarea onFocus="this.select()" cols="100" rows="10">
    {}
    </textarea> 
</div>
</fieldset>

<fieldset>
<legend>Le matériel</legend>
<div>
    <textarea onFocus="this.select()" cols="100" rows="10">
    {}
    </textarea> 
</div>
</fieldset>

<fieldset>
<legend>Le réseau</legend>
<div>
    <textarea onFocus="this.select()" cols="100" rows="10">
    {}
    </textarea> 
</div>
</fieldset>

<fieldset>
<legend>Les disques</legend>
<div>
    <textarea onFocus="this.select()" cols="100" rows="10">
    {}
    </textarea> 
</div>
</fieldset>

<fieldset>
<legend>Toutes les informations</legend>
<div>
    <textarea name="code" onFocus="this.select()" cols="100" rows="10">
    {}
    </textarea> 
</div>
</fieldset>
 
<input name="poster" type="hidden" value="DFrocks">        
<input name="expire" type="hidden" value="-1">        
<input name="lang" type="hidden" value="text">        
<input name="private" type="hidden" value=0>        

</form>

</main>
</body>
</html>
"""

css="""
<style type="text/css">
        body {
            margin: 0;
            width:100%;
            background-color:#eee;
            color:black;
            font-size:16px;
        }
        main {
            width:70%;
            text-align:center;
            margin:auto;
        }
        h1 { 
            color:#3876A8;
            text-transform:uppercase;
        }
</style>
"""

def gui():
    htmlfile = tempfile.mkstemp(prefix="info4df", suffix=".html")[1]
    
    distro_info = get_distro()
    hw_info = get_hw()
    nw_info = get_network()
    disks_info = get_disks()
    all_info = "\n".join([distro_info, hw_info, nw_info, disks_info])
    
    html = tpl.format(css , distro_info, hw_info, nw_info, disks_info, all_info )
    with open(htmlfile, "w") as out:
        out.write(html)
    webbrowser.open('file://{}'.format(htmlfile))
   
def main():
    gui()    
    
    return 0

if __name__ == '__main__':
    main()


