#!/usr/bin/env python
# -*- coding:Utf-8 -*-

# require python3-tk ou python-tk

from info4df import *

if sys.version[0] == "2":
        import Tkinter as tkinter
        import ttk
        from tkMessageBox import *
else:
        import tkinter
        from tkinter import ttk
        from tkinter.messagebox import *

        
class Guitk():
    def __init__(self, code):

        self.code = code
        
        self.root = tkinter.Tk()
        
        label = tkinter.Label(self.root, text="Voici les informations sur votre installation à copier", fg="green")
        label.pack()

        # create a Frame for the Text and Scrollbar
        txt_frm = tkinter.Frame(self.root, width=450, height=400)
        txt_frm.pack(fill="both", expand=True)
        # ensure a consistent GUI size
        txt_frm.grid_propagate(False)
        # implement stretchability
        txt_frm.grid_rowconfigure(0, weight=1)
        txt_frm.grid_columnconfigure(0, weight=1)

        # create a Text widget
        text = tkinter.Text(txt_frm, borderwidth=3, relief="sunken")
        text.config(font=("Monospace", 12), undo=True, wrap='word')
        text.grid(row=0, column=0, sticky="nsew", padx=2, pady=2)
        text.insert(tkinter.INSERT, code)

        # create a Scrollbar and associate it with txt
        scrollb = tkinter.Scrollbar(txt_frm, command=text.yview)
        scrollb.grid(row=0, column=1, sticky='nsew')
        text['yscrollcommand'] = scrollb.set
        
        
        # frame for buttons
        btn_frm = tkinter.Frame(self.root, width=450, height=400)
        btn_frm.pack(fill="both", expand=True,padx=20, pady=20)
        
        copybtn = tkinter.Button(btn_frm)
        copybtn.config(text='Copier dans le presse-papier', command=self.copy_code)
        copybtn.grid(row=0, column=0, padx=5, pady=5)
        
        pastebtn = tkinter.Button(btn_frm)
        pastebtn.config(text='Envoyer sur un pastebin', command=self.pastebin_code)
        pastebtn.grid(row=0, column=2, padx=5, pady=5)

        
    def copy_code(self):
        self.root.clipboard_clear()
        self.root.clipboard_append(self.code)
        
    def pastebin_code(self):
        paste_url = paste_code(self.code.replace('[code]','').replace('[/code]','').replace('[code=apt_sources]', ''))
        showinfo("Adresse du pastebin", paste_url)
        
    def run(self):
        self.root.mainloop()

def main():
    app = Guitk(get_all())
    app.run()
    return 0

if __name__ == '__main__':
    main()
