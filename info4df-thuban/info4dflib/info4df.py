#!/usr/bin/env python
# -*- coding:Utf-8 -*-


"""
Auteurs :            thuban (thuban@yeuxdelibad.net)    
                             arpinux (arpinux@member.fsf.org)
                             FirePowi
                             Michael Gebetsroither <michael@mgeb.org> (paste part)
licence :            GNU General Public Licence v3

Description : Outil pour lister une description du matériel
                        permettant d'être plus facilement dépanné en
                        cas de soucis.
                     
"""

"""
Nécessaire d'être root pour :
    fdisk
    dmidecode
"""

import os
import sys
if sys.version[0] == "2":
        from urllib import urlencode
        from urllib import FancyURLopener
else:
        from urllib.parse import urlencode
        from urllib.request import FancyURLopener

def myinput(txt):
    if sys.version_info >= (3, 0):
        rep = input(txt)
    else:
        rep = raw_input(txt)
    return(rep)

def upload_code(code):
        """upload code to paste.debian.net
        return the page url or False
        """
        DEFAULT_SERVER='https://paste.debian.net/'
        
        user = "DFrocks" 
        expire = -1
        lang = "text"
        private = 0

        params = { "poster":user, "expire":expire, "lang": lang, "private" : private, "code" : code }
        params = urlencode(params)

        url_opener = FancyURLopener()
        try:
            page = url_opener.open(DEFAULT_SERVER, params)
            #page_url = page.read().decode('utf-8').strip()
            return(page.url)
        except Exception as e:
            print("Échec à l'envoi du code sur le pastebin: \n{}".format(e)) 
            return(False)


def paste_code(code):
    paste_url = upload_code(code)
    if paste_url:
        import webbrowser
        webbrowser.open(paste_url)
        return(paste_url)

def bb(txt, title="",codetype=""):
    if len(txt) > 0:
        # put txt in bbcode
        if codetype != "":
            out = "--{}--\n[code={}]{}[/code]\n".format(title,codetype,txt)
        else:
            out = "--{}--\n[code]{}[/code]\n".format(title,txt)
    else:
        out = ""
    return(out)
 

def get_distro():
    # Infos sur la distribution
 
    # La version
    if os.path.isfile('/etc/debian_version'):
        with open('/etc/debian_version', 'r') as hi:
            debver = hi.read()
    else:
        debver = "unknown"
 
    # Nom de la distro
    host = os.popen("uname -n").read()
    # Version du kernel
    kernel = os.popen("uname -sr").read()
 
    # Sources.list
    sources = ""
    if os.path.isfile('/etc/apt/sources.list'):
        with open('/etc/apt/sources.list', 'r') as s:
            sources = "/etc/apt/sources.list:\n{}".format(s.read())
            for sourcefile in os.listdir('/etc/apt/sources.list.d'):
                if sourcefile.endswith('.list'):
                    with open(os.path.join('/etc/apt/sources.list.d',sourcefile), 'r') as s:
                        sources += "#/etc/apt/sources.list.d/{}\n{}".format(sourcefile,s.read())
    
    # Preferences
    preferences = ""
    if os.path.isfile('/etc/apt/preferences'):
        with open('/etc/apt/preferences', 'r') as pref:
            preferences += "/etc/apt/preferences:\n{}".format(pref.read())
    if os.path.isdir('/etc/apt/preferences.d'):
        for f in os.listdir('/etc/apt/preferences.d'):
            ff = os.path.join('/etc/apt/preferences.d',f)
            with open(ff, 'r') as pref:
                preferences += "{}:\n{}".format(ff,pref.read())
            
             
    disttxt = bb(debver, "Votre version de debian")
    disttxt += bb(host, "Nom de la distribution")
    disttxt += bb(kernel, "Version du noyau")
    disttxt += bb(sources,"Sources de paquets","apt_sources")
    disttxt += bb(preferences,"Préférences pour le pinning")
    return(disttxt)
 
def get_hw():
    pci = os.popen("lspci").read()
    usb = os.popen("lsusb").read()
    hw = os.popen("dmidecode -q -t bios -t system -t processor -t memory").read()
 
    hw_txt = bb(pci, "lspci - liste des périphériques internes")
    hw_txt += bb(usb, "usb - liste des périphériques connectés")
    hw_txt += bb(hw, "Informations sur votre matériel")
 
    return(hw_txt)
 
 
def get_network():
    with open('/etc/network/interfaces', 'r') as ni:
        netinterfaces = ni.read()
    with open('/etc/resolv.conf', 'r') as r:
        resolvconf = r.read()

    ifconfig = os.popen("/sbin/ifconfig -a").read()
    route = os.popen("/sbin/route -n").read()
 
    nw_txt = bb(netinterfaces, "/etc/network/interfaces")
    nw_txt += bb(resolvconf, "/etc/resolv.conf")
    nw_txt += bb(ifconfig, "ifconfig")
    nw_txt += bb(route, "route")
 
    return(nw_txt)
 
 
def get_disks():
    fs = os.popen("df -h").read()
    part = os.popen("/sbin/fdisk -l").read()
    with open('/etc/fstab', 'r') as f:
        fstab = f.read()
 
    txt = bb(fs, "Utilisation des disques")
    txt += bb(part, "Partitionnement des disques")
    txt += bb(fstab, "Tableau des systèmes de fichier")
 
    return(txt)

 

def get_all():
    info = get_distro()
    info += get_hw()
    info += get_network()
    info += get_disks()
    
    return(info)

def main():
    
    
    return 0

if __name__ == '__main__':
    main()


# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4
