#!/usr/bin/env python
# -*- coding:Utf-8 -*-

from info4df import *


class colors:
        NORMAL = '\033[0m'
        BOLD = '\033[1m'
        UNDERLINE = '\033[4m'
        GREEN="\033[32m"
        RED="\033[31m"
        CYAN="\033[36m"
        YELLOW="\033[33m"



t_intro=\
"""{}{}
Cette page vous permettra de transmettre des informations à propos de votre système Debian GNU/Linux pour obtenir de l'aide sur les forums.
Selon votre choix, le mot de passe superadministrateur (root) pourra vous être demandé.
    - Le texte sera pré-formaté, il vous suffit de le "copier/coller" sur le forum.
    - Vous pouvez aussi donner le lien qui vous sera donné : il pointe vers le compte-rendu ☺ .{}""".format(colors.GREEN,colors.BOLD, colors.NORMAL)


def cli():
    rep, info = "", ""
    print(t_intro)
    print(colors.GREEN)
    print("\t---\n\tQuelles informations souhaitez vous?\n\t---")
    print(colors.NORMAL)
    print("\t{}1{} : informations sur la distribution".format(colors.YELLOW, colors.NORMAL))
    print("\t{}2{} : informations sur le matériel".format(colors.YELLOW, colors.NORMAL))
    print("\t{}3{} : informations sur le réseau".format(colors.YELLOW, colors.NORMAL))
    print("\t{}4{} : informations sur les disques".format(colors.YELLOW, colors.NORMAL))
    print("\t{}5{} : toutes les informations".format(colors.YELLOW, colors.NORMAL))
    print("\t---")
    rep = myinput("[validez votre choix avec Entrée] > ")
    
    if rep == "1":
        info = get_distro()
    elif rep == "2":
        info = get_hw()
    elif rep == "3" :
        info = get_network()
    elif rep == "4" :
        info = get_disks()
    elif rep == "5":
        info = get_all()
    else:
        info = "Choix inconnu"
        sys.exit()
    
    print("\n---\n{}Voici le code à copier/coller sur le forum {}(ctrl-shift-c){} :".format(colors.CYAN, colors.GREEN,colors.NORMAL)) 
    print(info)
    
    # upload to a paste
    topaste = ""
    topaste = myinput("Voulez vous coller ces informations en ligne et récupérer le lien? (sur un pastebin) [o/N]\n> ")
    if topaste.lower() == "o":
        paste_url = paste_code(info.replace('[code]','').replace('[/code]','').replace('[code=apt_sources]', ''))
        if paste_url:
            print("{}Copiez ce lien qui contient toutes les informations ci-dessus :{}").format(colors.CYAN, colors.NORMAL) 
            print(paste_url)
    
    rep = myinput("... Appuyez sur Entrée pour quitter")

def help():
    print("usage : {} <options>".format(sys.argv[0]))
    print("           --gui    :    Affiche les informations dans un navigateur")
