-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: info4df
Binary: info4df
Architecture: all
Version: 0.1-1
Maintainer: team handylinux <dev@handylinux.org>
Homepage: https://debian-facile.org
Standards-Version: 3.9.6
Build-Depends: debhelper (>= 8.0.0)
Package-List:
 info4df deb x11 extra arch=all
Checksums-Sha1:
 8b0c4019efda11f765e8890ba6b21c86492bd39e 13268 info4df_0.1.orig.tar.xz
 615b52fa67cc98ab93d0222698e2805710a3445a 1564 info4df_0.1-1.debian.tar.xz
Checksums-Sha256:
 db6e176efbe2a3df6f80c1923ef369f25171b874bf7f925bf087e7057bd70fc8 13268 info4df_0.1.orig.tar.xz
 06777a4c7d7d5a5b25d6594f326e6a38de0ea57180759490fe65bfde8df84c99 1564 info4df_0.1-1.debian.tar.xz
Files:
 d8232e3cd6ed1d8ffe648b9d727dae60 13268 info4df_0.1.orig.tar.xz
 41f8f91424ca7ae670abf5c5cc8a22e0 1564 info4df_0.1-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQEcBAEBAgAGBQJXdwSxAAoJELOLTsK+RB/P8YcH/0XzEmM/mSkTarJszr7x/2Ov
Ne/LqFZsCzAcoDn/blW+FqdF0U8XQxlfXXI4Rw7qJ+y/2uN7Aq+obt1x7QDeEgrT
X99+t0soVBR6fW0e4r01SgdbCYKJaTkD1nIJUuAv8EQsNZ0DgorwbTL4F8gh/JtG
YWpUiJ/Pta04QkMTqqeqjayW0qtuEOY+BFUkcgb/ASZE1kd2KcbuRXB5M5vQO1zE
b0pcXJzNsbS6jq7307DINxZ5qv7O1udYsMMWK15GhHGuc6f3Q04a+GQbeRp3p4Bf
vY+zWc2GtCj+sfJPy5I2OETUSsFMSwsNG8efUld9c3C8B9EYt6raE6T76EHmrxs=
=SehK
-----END PGP SIGNATURE-----
